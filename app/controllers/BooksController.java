package controllers;

import models.Book;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

import views.html.books.create;
import views.html.books.index;
import views.html.books.edit;
import views.html.books.show;
import views.html.errors._404;

import javax.inject.Inject;

public class BooksController extends Controller {


    @Inject
    FormFactory formFactory;

    //for all books

    public Result index(){
        List<Book> books = Book.find.all();
        return ok(index.render(books));
    }

    //to create book

    public Result create(){
        Form<Book> bookForm = formFactory.form(Book.class);
        return ok(create.render(bookForm));
    }

    //saving book

    public Result save(){
        Form<Book> bookForm = formFactory.form(Book.class).bindFromRequest();

        if(bookForm.hasErrors()){
            flash("danger", "Please correct the form below");
            return badRequest(create.render(bookForm));
        }

        Book book = bookForm.get();
        book.save();

        flash("success", "Book created successfully");
        return redirect(routes.BooksController.index());
    }

    public Result edit(Integer id){

        Book book = Book.find.byId(id);

        if(book == null){
            return notFound(_404.render());
        }

        Form<Book> bookForm = formFactory.form(Book.class).fill(book);

        return ok(edit.render(bookForm));
    }

    public Result update(){
        Form<Book> bookForm= formFactory.form(Book.class).bindFromRequest();

        if(bookForm.hasErrors()){
            flash("danger", "Please correct the form below");
            return badRequest(edit.render(bookForm));
        }

        Book book = bookForm.get();
        Book oldBook = Book.find.byId(book.id);

        if(oldBook == null){
            flash("danger", "Book not found");
            return notFound(_404.render());
        }

        oldBook.title = book.title;
        oldBook.author = book.author;
        oldBook.price = book.price;

        oldBook.update();


        flash("success", "Book updated successfully");
        return ok();
    }

    //for book details

    public Result show(Integer id){
        Book book = Book.find.byId(id);
        if(book == null){
            return notFound(_404.render());
        }

        return ok(show.render(book));
    }


    public Result destroy(Integer id){
        Book book = Book.find.byId(id);
        if(book == null){
            flash("danger", "Book not found.");
            return notFound(_404.render());
        }

        book.delete();

        flash("success", "Book deleted");
        return ok();
    }

}
