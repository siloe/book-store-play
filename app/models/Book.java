package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book extends Model {

    @Id
    public Integer id;
    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(255)
    public String title;
    @Constraints.Required
    @Constraints.Min(0)
    public Integer price;
    public String author;

    public static Finder<Integer, Book> find = new Finder<Integer, Book>(Book.class);
}
